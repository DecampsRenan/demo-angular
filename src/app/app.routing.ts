import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Demo1Component } from './demo-1/demo1.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { Demo2Component } from './demo-2/demo2.component';
import { ParentComponent } from './demo-3/parent.component';
import { NotFoundComponent } from './not-found.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
