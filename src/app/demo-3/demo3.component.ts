import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo3.component.html',
})
export class Demo3Component implements OnInit, OnDestroy {

  @Input() counter = 0;
  @Input() timeout = 1000;

  private intervalRef;

  ngOnInit() {
    this.intervalRef = setInterval(() => this.counter++, this.timeout);
  }

  ngOnDestroy() {
    clearInterval(this.intervalRef);
  }

}
