import { ParentComponent } from './parent.component';
import { Demo3Component } from './demo3.component';
import { NgModule } from '@angular/core';
import { Demo3RoutingModule } from './demo3.routing';

@NgModule({
  imports: [
    Demo3RoutingModule,
  ],
  declarations: [
    Demo3Component,
    ParentComponent,
  ],
})
export class Demo3Module { }
