import { ParentComponent } from './parent.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo3Component } from './demo3.component';

const appRoutes: Routes = [
  {
    path: 'demo-3',
    component: ParentComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class Demo3RoutingModule {}
