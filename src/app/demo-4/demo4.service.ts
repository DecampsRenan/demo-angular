import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const BASE_URL_API = 'https://rickandmortyapi.com/api';

export interface Character {
  id: number;
  name: string;
  status: string;
  species: string;
  gender: string;
  image: string;
}
export type Characters = Character[];

@Injectable()
export class Demo4Service {

  constructor(
    private http: HttpClient,
  ) {}

  findAllRickAndMortyCharacters(): Observable<Characters> {
    return this.http.get(`${BASE_URL_API}/character`)
      .map(requestData => requestData['results']);
  }

}
