import { ParentComponent } from './parent.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo4Component } from './demo4.component';

const appRoutes: Routes = [
  {
    path: 'demo-4',
    component: ParentComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class Demo4RoutingModule {}
