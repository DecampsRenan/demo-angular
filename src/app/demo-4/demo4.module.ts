import { ParentComponent } from './parent.component';
import { Demo4Component } from './demo4.component';
import { NgModule } from '@angular/core';
import { Demo4RoutingModule } from './demo4.routing';
import { Demo4Service } from './demo4.service';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    Demo4RoutingModule,
  ],
  declarations: [
    Demo4Component,
    ParentComponent,
  ],
  providers: [
    Demo4Service,
  ],
})
export class Demo4Module { }
