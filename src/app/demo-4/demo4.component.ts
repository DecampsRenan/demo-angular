import { Characters } from './demo4.service';
import { Component, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo4.component.html',
})
export class Demo4Component {

  @Input() characters: Characters = [];

}
