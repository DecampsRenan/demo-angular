import { Characters, Demo4Service } from './demo4.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  template: `
    <p *ngIf="isRequestLoading">Loading characters...</p>
    <app-demo *ngIf="!isRequestLoading" [characters]="characters"></app-demo>
  `,
})
export class ParentComponent implements OnInit {

  characters: Characters = [];
  isRequestLoading = false;

  constructor(
    private demo4Service: Demo4Service,
  ) {}

  ngOnInit() {
    this.isRequestLoading = true;
    this.demo4Service.findAllRickAndMortyCharacters()
      .subscribe(
        (fetchedCharacters) => {
          this.characters = fetchedCharacters;
          this.isRequestLoading = false;
        },
        (error) => {
          console.warn('Oups...', error);
          this.isRequestLoading = false;
        }
      );
  }
}
