import { Component } from '@angular/core';

@Component({
  template: `
    <div>
      <h1>Hello {{name}}</h1>
    </div>
  `,
})
export class Demo1Component {
  name = 'World';
}
