import { Demo1Component } from './demo1.component';
import { NgModule } from '@angular/core';
import { Demo1RoutingModule } from './demo1.routing';

@NgModule({
  imports: [
    Demo1RoutingModule,
  ],
  declarations: [
    Demo1Component,
  ],
  entryComponents: [
    Demo1Component,
  ],
})
export class Demo1Module {}
