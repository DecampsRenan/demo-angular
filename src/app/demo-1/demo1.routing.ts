import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo1Component } from './demo1.component';

const appRoutes: Routes = [
  {
    path: 'demo-1',
    component: Demo1Component,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class Demo1RoutingModule {}
