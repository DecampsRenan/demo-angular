import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { Demo1Module } from './demo-1/demo1.module';
import { Demo2Module } from './demo-2/demo2.module';
import { Demo3Module } from './demo-3/demo3.module';
import { Demo4Module } from './demo-4/demo4.module';

import { AppRoutingModule } from './app.routing';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    Demo1Module,
    Demo2Module,
    Demo3Module,
    Demo4Module,
    AppRoutingModule,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }
