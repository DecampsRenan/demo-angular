import { Component } from '@angular/core';

@Component({
  template: `
    <h1>Nice try !</h1>
    <small>But there is nothing here...</small>
  `,
})
export class NotFoundComponent {}
