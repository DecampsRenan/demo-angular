import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  templateUrl: './demo2.component.html',
})
export class Demo2Component implements OnInit, OnDestroy {

  counter = 0;

  private intervalRef;

  ngOnInit() {
    this.intervalRef = setInterval(() => {
      this.counter++;
    }, 1000);
  }

  ngOnDestroy() {
    clearInterval(this.intervalRef);
  }

}
