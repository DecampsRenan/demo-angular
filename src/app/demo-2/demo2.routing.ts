import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Demo2Component } from './demo2.component';

const appRoutes: Routes = [
  {
    path: 'demo-2',
    component: Demo2Component,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class Demo2RoutingModule {}
