import { Demo2Component } from './demo2.component';
import { NgModule } from '@angular/core';
import { Demo2RoutingModule } from './demo2.routing';

@NgModule({
  imports: [
    Demo2RoutingModule,
  ],
  declarations: [
    Demo2Component,
  ],
})
export class Demo2Module { }
